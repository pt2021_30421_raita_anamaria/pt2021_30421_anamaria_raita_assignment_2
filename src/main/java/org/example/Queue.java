package org.example;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
/*This class is used to created the queues.It has different attributes which assure the threads safety.
* Each method will be explained above, except the getter for WaitFor time, which represents
* how many time units is the waiting time at that queue.
* And also, the toString method is straight forward, it prints the Queue number and the queue client list.
* */
public class Queue implements Runnable {
    private BlockingQueue<Client> clientQueue;
    private AtomicInteger maxClientsQueue;
    private AtomicInteger waitFor;
    private AtomicInteger numberQueue;
/*This method sets to the waiting time for the queue a the new waiting time,
 considering the service Time for the new client and adds the client to the queue.*/
    public void addToQueue(Client client) {
        this.waitFor.set(this.waitFor.get() + client.getServiceTime());
        this.clientQueue.add(client);
    }
/*This method selects the “peek” (first) element in the queue,
 if it’s not null, meaning there are still clients in the queue,
 it removes the first element and sets the new waiting time for the queue
  by subtracting the service Time for the client to be removed.*/
    public void removeFromQueue() {
        Client client = this.clientQueue.peek();
        if (client != null) {
            this.waitFor.set(this.waitFor.get() - client.getServiceTime());
            this.clientQueue.remove(client);
        }
    }
/*This is the constructor who constructs an object and starts a new thread for each queue*/
    public Queue(int qNb, int maxClientsQueue) {
        clientQueue = new LinkedBlockingQueue<>(maxClientsQueue);
        this.numberQueue = new AtomicInteger(qNb);
        this.waitFor = new AtomicInteger(0);
        this.maxClientsQueue = new AtomicInteger(maxClientsQueue);
        new Thread(this).start();
    }

    public AtomicInteger getWaitFor() {
        return waitFor;
    }

/*This method shows what happens in each queue (thread).
 As long as there are clients at the queue,
 the thread sleeps the amount of time equal with the service time of the client,
 after that meaning that the client was processed and that client is removed from the queue.*/
    public void run() {
        while (true) {
            if (!this.clientQueue.isEmpty()) {
                try {
                    Thread.sleep(1000 * this.clientQueue.element().getServiceTime());

                    removeFromQueue();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Queue" + numberQueue +
                "    " + clientQueue + "\n";
    }

    public BlockingQueue<Client> getClientQueue() {
        return clientQueue;
    }
}
