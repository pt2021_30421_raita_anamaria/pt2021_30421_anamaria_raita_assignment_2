package org.example;

import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;

public class View {
    private JButton startSimulationButton;
    private JTextField currentTime;
    private JTextField queues;
    private JTextField clients;
    private JTextField mat;
    private JTextField maxat;
    private JTextField mst;
    private JTextField maxst;
    private JTextArea textArea1;
    private JPanel main;
    private JTextField sim;
    private int cozi;
    private int clienti;
    private int minA;
    private int maxA;
    private int minS;
    private int maxS;
    private int simTime;


    public JTextField getCurrentTime() {
        return currentTime;
    }

    public JPanel getMain() {
        return main;
    }

    public View() {
        queues.setText("3");
        clients.setText("10");
        mat.setText("1");
        maxat.setText("6");
        mst.setText("2");
        maxst.setText("4");
        sim.setText("20");
    }
/* This 3  methods are used in order to print in file the log of events and also on the interface*/
    public void appendTextToTextArea(String logEvent) {
        addTextToFile(logEvent);
        this.textArea1.append(logEvent);
    }

    public void setTextInArea(String output) {
        addTextToFile(output);
        this.textArea1.setText(output);
    }

    private void addTextToFile(String text) {
        try (FileWriter fileWriter = new FileWriter("src/main/resources/output.txt", true)) {
            fileWriter.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public JButton getStartSimulationButton() {
        return startSimulationButton;
    }

    public JTextField getQueues() {
        return queues;
    }

    public JTextField getClients() {
        return clients;
    }

    public JTextField getMat() {
        return mat;
    }

    public JTextField getMaxat() {
        return maxat;
    }

    public JTextField getMst() {
        return mst;
    }

    public JTextField getMaxst() {
        return maxst;
    }

    public JTextField getSim() {
        return sim;
    }

    public int getCozi() {
        return cozi;
    }

    public int getClienti() {
        return clienti;
    }

    public int getMinA() {
        return minA;
    }

    public int getMaxA() {
        return maxA;
    }

    public int getMinS() {
        return minS;
    }

    public int getMaxS() {
        return maxS;
    }

    public int getSimTime() {
        return simTime;
    }


    public void setCozi(int cozi) {
        this.cozi = cozi;
    }

    public void setClienti(int clienti) {
        this.clienti = clienti;
    }

    public void setMinA(int minA) {
        this.minA = minA;
    }

    public void setMaxA(int maxA) {
        this.maxA = maxA;
    }

    public void setMinS(int minS) {
        this.minS = minS;
    }

    public void setMaxS(int maxS) {
        this.maxS = maxS;
    }

    public void setSimTime(int simTime) {
        this.simTime = simTime;
    }
}
