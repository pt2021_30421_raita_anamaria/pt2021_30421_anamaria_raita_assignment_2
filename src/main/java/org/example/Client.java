package org.example;

import java.util.Objects;
/*
* This class is pretty simple and straight forward.
* It has the specified attributes for the client:id, arrival and service time.
* It also has two getters for the time attributes.
* A constructor for the object client.
* A toString method who prints the client with its attributes.
* A compareTo method for ordering in ascending order the clients by their arrivalTime.
* The equals method to help us compare 2 client objects.
*
* */
public class Client implements Comparable<Client> {
    private int clientID;
    private int arrivalTime;
    private int serviceTime;

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public Client(int clientID, int arrivalTime, int serviceTime) {
        this.clientID = clientID;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    @Override
    public int compareTo(Client o) {
        return this.arrivalTime - o.arrivalTime;
    }

    @Override
    public String toString() {
        return "Client(" + clientID + "," + arrivalTime + "," + serviceTime +
                ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return clientID == client.clientID &&
                arrivalTime == client.arrivalTime &&
                serviceTime == client.serviceTime;
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientID, arrivalTime, serviceTime);
    }
}
