package org.example;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private int nbOfQueues;
    private ArrayList<Queue> queueArrayList;
    public float total=0;
    public ArrayList<Queue> getQueueArrayList() {
        return queueArrayList;
    }
    int totalPr=0;

/*The constructor creates a new queue until the number of queues from the interface is accomplished
and it adds the queue to the ArrayList of queues.*/
    public Scheduler(int nbOfQueues) {
        int iterator;
        this.nbOfQueues = nbOfQueues;
        this.queueArrayList = new ArrayList<>();
        for (iterator = 0; iterator < this.nbOfQueues; iterator++) {
            Queue queue = new Queue(iterator, 50);
            this.queueArrayList.add(queue);
        }
    }


    /*This method, as long as we have the clients waiting to be placed at the queue,
   goes through the arrayList of queues and finds what queue has the minimum waiting time.
    Then, it places the client at that certain queue and it also removes the client
     from the list of clients waiting to be placed in the line.*/
    public void addClientsToQueue(List<Client> clients) {
        int min = 214748364;
        Queue qResult = null;
        totalPr=totalPr+ clients.size();
        while (!clients.isEmpty()) {
            for (Queue q : this.queueArrayList) {
                if (q.getWaitFor().get() <= min) {
                    min = q.getWaitFor().get();
                    qResult = q;

                }
            }
            for (Client cl: qResult.getClientQueue()) {
                if(!cl.equals(clients.get(0))){
                    total=total+cl.getServiceTime();
                }
            }
            qResult.addToQueue(clients.remove(0));
        }
    }


/*This method is used to display the existing clients in each queue,
going through each queue in the arrayList.*/
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Queue queue : queueArrayList) {
            result.append(queue.toString());
        }
        return result.toString();
    }

    public float getTotal() {
        return total;
    }

    public int getTotalPr() {
        return totalPr;
    }
}
