package org.example;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class SimulationController implements Runnable {
    private static final Random RANDOM = new Random();
    private List<Client> clients;
    private Scheduler scheduler;
    private int nrOfClients;
    private int minArriveTime;
    private int maxArriveTime;
    private int minServiceTime;
    private int maxServiceTime;
    private int simulationTime;
    private AtomicInteger currentTime;
    private static View v = new View();
    private float sum=0;
    private float mediaService=0;
    int nr=0;
    int maxim=0;
    int current=0;
/*This is the constructor where we consider all the data input required in the interface */
    public SimulationController(int nrOfClients, int minArriveTime, int maxArriveTime, int minServiceTime, int maxServiceTime, int simulationTime, int nrOfQueues) {
        this.clients = new ArrayList<>(nrOfClients);
        this.nrOfClients = nrOfClients;
        this.minArriveTime = minArriveTime;
        this.maxArriveTime = maxArriveTime;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
        this.simulationTime = simulationTime;
        this.scheduler = new Scheduler(nrOfQueues);
        this.currentTime = new AtomicInteger(0);

    }


    /*This method generates, for each client a random arrival and service time,
    adds it to the clients list and after that it sorts them considering the arrival time.*/
    public void generateClients() {
        for (int i = 0; i < nrOfClients; i++) {
            int arrivalTime = generateRandomInt(minArriveTime, maxArriveTime);
            int serviceTime = generateRandomInt(minServiceTime, maxServiceTime);
            clients.add(new Client(i, arrivalTime, serviceTime));
        }
        Collections.sort(clients);
    }
/*This method generates a random value between 2 bounds*/
    private int generateRandomInt(int min, int max) {
        return RANDOM.nextInt(max - min) + min;
    }


    /*As long as the current time is smaller that the simulation time,
     I create a list with people who have the same arrival time and equal with the current time.
     I remove them from the clients which are waiting to be dispatched.
      Then I add them to the queue. I increment the current time and the thread sleeps for one second,
       so it is synchronized with the current time.*/
    @Override
    public void run() {

        v.setTextInArea(clients.toString());
        while (currentTime.get() <= simulationTime) {
            List<Client> highestArrivalTime = clients.stream().filter(client -> client.getArrivalTime() == currentTime.get()).collect(Collectors.toList());
            for (int i=0;i< highestArrivalTime.size();i++){
             sum=sum+highestArrivalTime.get(i).getServiceTime();
            }
            nr=nr+highestArrivalTime.size();
            clients.removeAll(highestArrivalTime);
            scheduler.addClientsToQueue(highestArrivalTime);
            v.setTextInArea(clients.toString());
            v.appendTextToTextArea("\n");
            v.appendTextToTextArea(scheduler.getQueueArrayList().toString());
            int totalq=0;
            for(Queue q:scheduler.getQueueArrayList()){
                totalq=totalq+q.getClientQueue().size();
            }
            if(totalq>maxim){
                maxim=totalq;
                current=currentTime.intValue();
            }
            currentTime.incrementAndGet();
            v.getCurrentTime().setText(currentTime.toString());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mediaService=sum/nr;
        v.appendTextToTextArea("\n"+"Average Service Time is "+mediaService);
        v.appendTextToTextArea("\n"+"Average waiting time is "+scheduler.getTotal()/scheduler.getTotalPr());
        v.appendTextToTextArea("\n"+"Peak hour is "+current);
    }
    /*This is the main method where I made all the validation for the input data
     and took over the data from the interface.*/
    public static void main(String[] args) {

        JFrame frame = new JFrame("Queues Simulator");
        frame.setContentPane(v.getMain());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        JOptionPane.showMessageDialog(null, "Insert integer values in the corresponding text fields!\n" +
                "If one of the input fields is not a numerical value, you will get an error!\n" +
                "The minimum values should be smaller than the maximum values!\n" +
                "If one of the time inputs is larger than the simulation time, you will also get an error!\n" +
                "You are permitted to do a single simulation, after that the buttons are disabled!\n");
        v.getStartSimulationButton().addActionListener(e -> {
            try {
                v.setCozi(Integer.parseInt(v.getQueues().getText()));
                v.setClienti(Integer.parseInt(v.getClients().getText()));
                v.setMinA(Integer.parseInt(v.getMat().getText()));
                v.setMaxA(Integer.parseInt(v.getMaxat().getText()));
                v.setMinS(Integer.parseInt(v.getMst().getText()));
                v.setMaxS(Integer.parseInt(v.getMaxst().getText()));
                v.setSimTime(Integer.parseInt(v.getSim().getText()));

                if (v.getMinA() > v.getMaxA()) {
                    JOptionPane.showMessageDialog(null, "Error!MinA > MaxA");
                }
                if (v.getMinS() > v.getMaxS()) {
                    JOptionPane.showMessageDialog(null, "Error!MinS > MaxS");
                }
                if (v.getMinA() < 0 || v.getMaxA() < 0 || v.getCozi() < 0 || v.getClienti() < 0 || v.getMinS() < 0 || v.getMaxS() < 0 || v.getSimTime() < 0) {
                    JOptionPane.showMessageDialog(null, "Negative numbers");
                }
                if (v.getMaxS() > v.getSimTime() || v.getMaxA() > v.getSimTime()) {
                    JOptionPane.showMessageDialog(null, "Interval Bounds greater than sim time");
                }
                SimulationController simulator = new SimulationController(v.getClienti(), v.getMinA(), v.getMaxA(), v.getMinS(), v.getMaxS(), v.getSimTime(), v.getCozi());
                simulator.generateClients();
                new Thread(simulator).start();

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error! Not integer values");
            }
        });
    }
}
